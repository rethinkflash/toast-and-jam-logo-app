//
//  PointTracer.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-09.
//
//

#ifndef PointTracer_h
#define PointTracer_h

#include "ofMain.h"

class PointTracer {
protected:
    ofRectangle* clickArea;
    
public:
    void setup();
    void draw(vector <vector<ofVec2f>> vertPoints);
    void pointClicked(int x, int y);
    void setCenter(int x, int y);
    void saveFile();

    ofVec2f screenCenter;
    vector <vector<ofVec2f>> vertPoints;
};

#endif /* PointTracer_h */
