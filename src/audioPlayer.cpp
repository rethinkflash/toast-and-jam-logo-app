//
//  audioPlayer.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-11.
//
//

#include "audioPlayer.h"

void AudioPlayer::setup() {
    
    fftSmooth = new float [8192];
    for(int i = 0; i < 8192; i++) {
        fftSmooth[i] = 0;
    }
    
    paused = true;
    prevPos = 0.0;
    
    initParams();
    
}

void AudioPlayer::update() {
    ofSoundUpdate();
    processfftSmooth();
    playheadSlider.set(musicPlayer.getPosition());
    
    if(playPauseBool){
        //checks to see if the song is finished or not
        if(prevPos >= 0.95 && musicPlayer.getPosition() == 0.0) {
            cout << "song has ended" << endl;
            paused = true;
            playPauseBool.setWithoutEventNotifications(false);
            restartHeadHandler();
        }
        
        prevPos = musicPlayer.getPosition();
    }
    
    if(browseFileBoolButton) {
        browseFileBoolButton.set(false);
        loadFileHandler();
    }
    
    if(restartBoolButton) {
        restartBoolButton.set(false);
        restartHeadHandler();
    }
}

void AudioPlayer::loadFileHandler() {
    ofFileDialogResult openFileResult = ofSystemLoadDialog("Select an audio file.");
    
    if(openFileResult.bSuccess) {
        processOpenFileSelection(openFileResult);
    }
}

void AudioPlayer::processOpenFileSelection(ofFileDialogResult openFileResult) {
    string filePath = openFileResult.getPath();
    string fileName = openFileResult.getName();
    
    ofFile file (openFileResult.getPath());
    
    if (file.exists()){
        string fileExtension = ofToUpper(file.getExtension());
        
        if (fileExtension == "WAV" || fileExtension == "MP3") {
            paused = false;
            musicPlayer.setPaused(paused);
            musicPlayer.stop();
            musicPlayer.unload();
            musicPlayer.load(filePath);
            if(autoPlayBool) {
                playPauseBool = true;
                musicPlayer.play();
            }
            currentFileLabel.set(fileName);
        }
    }
}

void AudioPlayer::processfftSmooth() {
    
    spectrumPoints.clear();
    
    
    float * value = ofSoundGetSpectrum(bands);
    
    for(proc_i = 0; proc_i < bands; proc_i++) {
        fftSmooth[proc_i] *= 0.96f;
        if(value[proc_i] > fftSmooth[proc_i]) {
            fftSmooth[proc_i] = value[proc_i]*0.97f;
        }
        spectrumPoints.push_back(fftSmooth[proc_i]);
    }
}

vector<float> AudioPlayer::getPointValues() {
    return spectrumPoints;
}

void AudioPlayer::playPauseHandler() {

    if(musicPlayer.isLoaded()){
        
        if(!musicPlayer.isPlaying()) {
            // if the file is loaded, but not yet playing, starts playing
            musicPlayer.play();
            playPauseBool.set(true);
            paused = false;
        } else {
            
            // if the audio is playing, this will toggle the pause of the audio
            if(paused) {
                playPauseBool.set(true);
                paused = false;
                
            } else {
                playPauseBool.set(false);
                paused = true;
            }
            musicPlayer.setPaused(paused);
        }
        
    } else {
        // is automatically set to false if no file is loaded
        playPauseBool.set(false);
    }
}

void AudioPlayer::restartHeadHandler() {
    if(musicPlayer.isLoaded()){
        musicPlayer.setPaused(true);
        musicPlayer.setPosition(0.0);
        musicPlayer.setPaused(paused);
    }

}

void AudioPlayer::initParams() {
    
    parameters.setName("Audio Player");
    
    parameters.add(browseFileBoolButton.set("Browse Audio File",false));
    parameters.add(currentFileLabel.set("File", "<none>"));
    parameters.add(autoPlayBool.set("Autoplay", true));
    
    parameters.add(playPauseBool.set("Play/Pause",false));
    parameters.add(restartBoolButton.set("Restart",false));
    parameters.add(playheadSlider.set("Playhead", 0, 0, 1));
    parameters.add(speedSlider.set("Speed", 1, -2, 2));
    parameters.add(volumeSlider.set("Volume", 0.8, 0, 1));
    parameters.add(bands.set("Band Count", 100, 60, 512));
    
    
    playPauseBool.addListener(this, &AudioPlayer::playPauseBoolEvent);
    
    volumeSlider.addListener(this, &AudioPlayer::setVolumeField);
    speedSlider.addListener(this, &AudioPlayer::setSpeedField);
    playheadSlider.addListener(this, &AudioPlayer::setPlayheadField);

    musicPlayer.setVolume(volumeSlider.get());
    musicPlayer.setSpeed(speedSlider.get());
    
}