#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxVideoRecorder.h"
#include "ofxSimpleTimer.h"
//#include "ofxGuiExtended.h"
#include "audioPlayer.h"
#include "logoPoints.h"
//#include "PointTracer.h"
#include "basicBeat.h"
#include "flowToolsDisplay.h"
#include "BeatDisplacementMap.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
        void setupGui();
		void update();
		void draw();
        void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        void takeScreenshot();
        void recordVideo();
        void stopRecording(string & name);
        void recordingComplete(ofxVideoRecorderOutputFileCompleteEventArgs& args);
        void audioIn(float * input, int bufferSize, int nChannels);



    void updateCenters();
    void resetPositions();
    
    bool showGuiBool;
    bool showGuiStore;
    
    bool screengrabBool;
    ofImage screengrabImg;
    ofVideoGrabber vidGrabber;
    ofxVideoRecorder vidRecorder;
    ofSoundStream soundStream;
    bool videoRecordingBool;
    ofxSimpleTimer *_timer;

    ofxPanel gui;
    
    ofParameterGroup parameters;
    
    ofParameterGroup positionParameters;
    ofParameter<int> centerX;
    ofParameter<int> centerY;
    void updateCentersHandler(int& _value) {
        updateCenters();
    }

//    ofxToggle showDebugToggle;

    ofParameter<float> centerXSlider;
    void setCenterXField(float& _value) {
        centerOffset.x = _value;
        updateCenters();
    }
    ofParameter<float> centerYSlider;
    void setCenterYField(float& _value) {
        centerOffset.y = _value;
        updateCenters();
    }

    bool debugBool;

    ofPoint centerOffset;
    ofRectangle centerOffsetBounds;

    AudioPlayer audioPlayer;

//    ofImage toastLogo;
    LogoPoints logoPoints;
//    PointTracer pointTracer;
    BasicBeat basicBeat;
    BeatDisplacementMap beatDisplacementMap;

    FlowToolsDisplay flowDisplay;
};
