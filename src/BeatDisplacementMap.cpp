//
//  BeatDisplacementMap.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-09-28.
//
//

#include "BeatDisplacementMap.h"

void BeatDisplacementMap::setup() {
    
    parameters.setName("Beat Disp. Map");
    parameters.add(displayToggle.set("Display",true));
    parameters.add(displayAlpha.set("Display Alpha",255, 0 ,255));
    
    parameters.add(blurOffset.set("Blur Offset", 10,0,20));
    parameters.add(blurPasses.set("Blur Passes", 5,0,10));
    parameters.add(numBlurOverlays.set("Num Blur Overlays", 1,0,5));
    parameters.add(blurOverlayGain.set("Blur Overlay Gain",255,0,255));
    
    int midX = int(ofGetWindowWidth()*0.5);
    int midY = int(ofGetWindowHeight()*0.5);
    
    parameters.add(centerOffsetX.set("Center Offset X",0,-midX,midX));
    parameters.add(centerOffsetY.set("Center Offset Y",0,-midY,midY));
    parameters.add(scale.set("Scale",1,0.1,5));
    
    
    centerOffsetX.addListener(this, &BeatDisplacementMap::updateCenterOffsetHandler);
    centerOffsetY.addListener(this, &BeatDisplacementMap::updateCenterOffsetHandler);
    
    allocatedFbos(ofGetWindowWidth(), ofGetWindowHeight());
    
}

void BeatDisplacementMap::setCenter(int x, int y) {
    screenCenter.set(x,y);
}

void BeatDisplacementMap::updateCenterOffset() {
    centerOffset.set(centerOffsetX,centerOffsetY);
}

void BeatDisplacementMap::update() {
    gpuBlur.blurOffset = blurOffset;
    gpuBlur.blurPasses = blurPasses;
    gpuBlur.numBlurOverlays = numBlurOverlays;
    gpuBlur.blurOverlayGain = blurOverlayGain;
}

void BeatDisplacementMap::draw(vector<float> audioPointsValues) {
    
    radius = 0.0;
    colorValue = 0;
    pointSize = audioPointsValues.size();
    maxDistance = ( ofGetWidth() < ofGetHeight() ) ? ofGetHeight() : ofGetWidth();
    falloff = 100;
    maxRadius = maxDistance * 0.5 + falloff;
    
    gpuBlur.beginDrawScene();
        ofPushStyle();
        ofEnableBlendMode(OF_BLENDMODE_ALPHA);
        ofClear(0,0,0,0);
        ofSetCircleResolution(25);
    
    for( point_i = (pointSize-1); point_i >= 0 ; point_i--) {

        radius = (float)ofMap(point_i, 0, pointSize, 10, maxRadius) * scale;

        colorValue = (int)ofMap(audioPointsValues.at(point_i), 0, 1.0, 0, 255);
        
        ofSetColor(colorValue,colorValue,colorValue,255);
        ofDrawCircle(screenCenter.x + centerOffset.x, screenCenter.y + centerOffset.y,radius);
    }
    
        ofPopStyle();
    gpuBlur.endDrawScene();
    
    //blur the fbo
    //blending will be disabled at this stage
    gpuBlur.performBlur();
    
    if(displayToggle) {
        ofPushStyle();
//        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA); //pre-multiplied alpha
        ofEnableBlendMode(OF_BLENDMODE_ALPHA);
        ofSetColor(255, 255, 255, displayAlpha);
        gpuBlur.drawBlurFbo(true);
        
        ofPopStyle();
//        ofDrawBitmapStringHighlight("MouseX to control Blur Passes\nMouseY to control blur Offset", 400,20);
    }

}

ofPixels* BeatDisplacementMap::getPixels() {
    gpuBlur.getBlurredSceneFbo().getTexture().readToPixels(pixels);
    return &pixels;
}

void BeatDisplacementMap::updateSize(int width, int height) {
    
    preX = centerOffsetX;
    preY = centerOffsetY;
    
    midX = int(width*0.5);
    midY = int(height*0.5);
    
    centerOffsetX.set("Center Offset X",preX,-midX,midX);
    centerOffsetY.set("Center Offset Y",preY,-midY,midY);
}

void BeatDisplacementMap::allocatedFbos(int width, int height) {
    ofFbo::Settings s;
    s.width = width;
    s.height = height;
    s.internalformat = GL_RGBA;
    s.textureTarget = GL_TEXTURE_RECTANGLE_ARB;
    s.maxFilter = GL_LINEAR; GL_NEAREST;
    s.numSamples = 0;
    s.numColorbuffers = 1;
    s.useDepth = false;
    s.useStencil = false;
    
    gpuBlur.setup(s, false);
}