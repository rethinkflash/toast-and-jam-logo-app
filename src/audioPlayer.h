//
//  audioPlayer.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-11.
//
//

#ifndef audioPlayer_h
#define audioPlayer_h

#include "ofMain.h"
#include "ofxGui.h"

class AudioPlayer {
protected:

    
public:
    void setup();
    void initParams();
    void update();
    void playPauseHandler();
    void restartHeadHandler();
    void loadFileHandler();
    void processOpenFileSelection(ofFileDialogResult openFileResult);
    
    void processfftSmooth();
	int proc_i;
    vector<float> getPointValues();
    
    ofParameterGroup parameters;

    ofParameter<string> currentFileLabel;
    
    ofParameter<bool> autoPlayBool;
    
    ofParameter<bool> browseFileBoolButton;
    
    ofParameter<bool> playPauseBool;
    void playPauseBoolEvent(bool& _value) {
        cout << "play pause event triggered" << endl;
        playPauseHandler();
    }
    
    ofParameter<bool> restartBoolButton;
    ofParameter<int> bands;
    
    
    bool paused;
    
    
    ofParameter<float> playheadSlider;
    void setPlayheadField(float& _value) {
        if(musicPlayer.isLoaded() && !musicPlayer.isPlaying()) {
            musicPlayer.play();
            paused = true;
            musicPlayer.setPaused(paused);
        }
        musicPlayer.setPosition(_value);
    }
    
    ofParameter<float> speedSlider;
    void setSpeedField(float& _value) {
        musicPlayer.setSpeed(_value);
    }
    
    ofParameter<float> volumeSlider;
    void setVolumeField(float& _value) {
        musicPlayer.setVolume(_value);
    }
    ofParameter<float> numberBandsSlider;
    void setNumberBandsField(float& _value) {
        bands = _value;
    }
    
    float prevPos;
    
    ofSoundPlayer musicPlayer;
    
    float * fftSmooth;
    
    vector<float> spectrumPoints;
};

#endif /* audioPlayer_h */
