//
//  logoPoints.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-10.
//
//

#include "logoPoints.h"

void LogoPoints::setup() {
    currentRowPos = -1;
    loadPoints();
}

void LogoPoints::startNewRow() {
    currentRowPos++;
    currentRow = new vector<ofVec2f>();
}

void LogoPoints::endNewRow() {
    vertPoints.push_back(*currentRow);
}

void LogoPoints::addPoint(int x, int y) {
//    ofLog() << x << ", " << y;
    currentRow->push_back(ofVec2f(x, y));
}

void LogoPoints::loadPoints() {
    
    vertPoints.clear();
    ofFile pointsFile;
    pointsFile.open(ofToDataPath("logopoints_top_to_bottom.xml"));
    ofBuffer buffer = pointsFile.readToBuffer();
    
    ofXml xmlPoints;
    xmlPoints.loadFromBuffer(buffer.getText());
    xmlPoints.setTo("points");
    int numRows = xmlPoints.getNumChildren();
    vertPoints.reserve(numRows);
    xmlPoints.setTo("row[0]");
    for(int i = 0; i < numRows; i++) {
        
        //        ofLog() << i << " X: " << xmlPoints.getValue("[@x]") << "; Y: " << xmlPoints.getValue("[@y]");
        int numPoints = xmlPoints.getNumChildren();
        int posY = xmlPoints.getIntValue("[@y]");
        xmlPoints.setTo("point[0]");
        startNewRow();
        for(int j = 0; j < numPoints; j++) {
            addPoint(xmlPoints.getIntValue("[@x]"),posY);
            xmlPoints.setToSibling();
        }
        endNewRow();
        xmlPoints.setToParent();
        xmlPoints.setToSibling();
    }
    pointsFile.close();
    
    
}

void LogoPoints::setCenter(int x, int y) {
    screenCenter.set(x,y);
}

vector < vector<ofVec2f> > LogoPoints::getPoints() {
    return vertPoints;
}

int LogoPoints::getRowCount() {
    return vertPoints.size();
}

int LogoPoints::getPointCount() {
    int count = 0;
    int rowSize = vertPoints.size();
//    vector<ofVec2f>* thisRow;
    for(int i = 0; i < rowSize; i++) {
        count += vertPoints[i].size();
//        thisRow = &vertPoints[i];
//        count += thisRow->size();
    }
    return count;
//    return vertPoints.size();
}