//
//  BeatDisplacementMap.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-09-28.
//
//

#ifndef BeatDisplacementMap_h
#define BeatDisplacementMap_h

#include "ofMain.h"
#include "ofxFboBlur.h"


class BeatDisplacementMap {
    
public:
    void setup();
    void update();
    void draw(vector<float> audioPointsValues);
    void setCenter(int x, int y);
    void updateCenterOffset();
    void updateSize(int width, int height);
    
    ofParameterGroup parameters;
    ofParameter<bool> displayToggle;
    ofParameter<int> displayAlpha;
    ofParameter<int> blurOffset;
    ofParameter<int> blurPasses;
    ofParameter<int> numBlurOverlays;
    ofParameter<int> blurOverlayGain;
    
    ofParameter<float> centerOffsetX;
    ofParameter<float> centerOffsetY;
    ofParameter<float> scale;
    
    void updateCenterOffsetHandler(float& _value) {
        updateCenterOffset();
    }
    
    ofPixels* getPixels();
    ofPixels pixels;
	
	float radius;
	int colorValue;
	int pointSize;
	int maxDistance;
	int falloff;
	int maxRadius;
	
	int point_i;
	
	int preX;
	int preY;
	int midX;
	int midY;
    
    ofVec2f screenCenter;
    ofVec2f centerOffset;
    ofxFboBlur gpuBlur;
    ofTexture tex;
    
private:
    void allocatedFbos(int width, int height);
    
};

#endif /* BeatDisplacementMap_h */
