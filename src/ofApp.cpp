#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	ofSetWindowTitle("Toast + Jam");

    ofSetVerticalSync(true);
    ofSetLogLevel(OF_LOG_NOTICE);
    
//    ofSetFullscreen(true);
	
//    ofSetFrameRate(30);
    
    debugBool = true;
    showGuiBool = true;
    screengrabBool = false;
    videoRecordingBool = false;

    centerOffset.set(0,0);
    centerOffsetBounds.set(-ofGetWindowWidth()/2, -ofGetWindowHeight()/2, ofGetWindowWidth(), ofGetWindowHeight());


    logoPoints.setup();

//    pointTracer.setup();
    beatDisplacementMap.setup();
	basicBeat.setup(logoPoints.getPoints(), beatDisplacementMap.centerOffset);

    flowDisplay.setup();

    audioPlayer.setup();
    setupGui();

    updateCenters();

}

void ofApp::setupGui(){
    
    int guiColorSwitch = 0;
    ofColor guiHeaderColor[4];
    ofColor guiFillColor[4];
    int guiAlpha = 200;
    
    //Audio Gui Colors
    guiHeaderColor[0].set(133, 7, 7, guiAlpha);
    guiFillColor[0].set(143, 67, 7, guiAlpha);
    
    //BasicBeat Gui Colors
    guiHeaderColor[1].set(133, 92, 7, guiAlpha);
    guiFillColor[1].set(120, 105, 0, guiAlpha);
    
    //BeatDisplacement Gui Colors
    guiHeaderColor[2].set(45, 85, 120, guiAlpha);
    guiFillColor[2].set(43, 44, 133, guiAlpha);
    
    //FlowTools Gui Colors
    guiHeaderColor[3].set(23, 94, 120, guiAlpha);
    guiFillColor[3].set(19, 133, 100, guiAlpha);
    
    float panelOffset = 180;
    
    gui.setup("Settings");
    gui.setDefaultBackgroundColor(ofColor(0,0,0,127));
    gui.setDefaultFillColor(ofColor(160,160,160,160));

    
    parameters.setName("settings");
    
    positionParameters.setName("Positioning");
    int midX = int(ofGetWindowWidth()*0.5);
    int midY = int(ofGetWindowHeight()*0.5);
    
    positionParameters.add(centerX.set("Center X",0,-midX,midX));
    positionParameters.add(centerY.set("Center Y",0,-midY,midY));
    
    //Audio player gui
    gui.setDefaultHeaderBackgroundColor(guiHeaderColor[0]);
    gui.setDefaultFillColor(guiFillColor[0]);
    gui.add(audioPlayer.parameters);
    
    //BasicBeat gui
    gui.setDefaultHeaderBackgroundColor(guiHeaderColor[1]);
    gui.setDefaultFillColor(guiFillColor[1]);
    basicBeat.parameters.add(positionParameters);
    gui.add(basicBeat.parameters);
    
    //BeatDisplacement gui
    gui.setDefaultHeaderBackgroundColor(guiHeaderColor[2]);
    gui.setDefaultFillColor(guiFillColor[2]);
    gui.add(beatDisplacementMap.parameters);
    
    //FlowTools gui
    gui.setDefaultHeaderBackgroundColor(guiHeaderColor[3]);
    gui.setDefaultFillColor(guiFillColor[3]);
    gui.add(flowDisplay.parameters);
    

    centerX.addListener(this, &ofApp::updateCentersHandler);
    centerY.addListener(this, &ofApp::updateCentersHandler);
    
    
	// if the settings file is not present the parameters will not be set during this setup
	if (!ofFile("settings.xml"))
		gui.saveToFile("settings.xml");
	
	gui.loadFromFile("settings.xml");
	gui.minimizeAll();
}



//--------------------------------------------------------------
void ofApp::update(){
    if(videoRecordingBool) {
        _timer->update();
        screengrabImg.grabScreen(0,0, ofGetWidth(), ofGetHeight());
        
        bool success = vidRecorder.addFrame(screengrabImg.getPixels());
        if (!success) {
            ofLogWarning("This frame was not added!");
        }
        // Check if the video recorder encountered any error while writing video frame or audio smaples.
        if (vidRecorder.hasVideoError()) {
            ofLogWarning("The video recorder failed to write some frames!");
        }
        
        if (vidRecorder.hasAudioError()) {
            ofLogWarning("The video recorder failed to write some audio samples!");
        }
    }
    audioPlayer.update();
    beatDisplacementMap.update();
	basicBeat.update(logoPoints.getPoints(), beatDisplacementMap.getPixels(), beatDisplacementMap.centerOffset);
    flowDisplay.update(basicBeat.getTexture());
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofClear(0,0);
//    if(showDebugToggle) {
//        toastLogo.draw(ofGetWindowWidth()/2 - toastLogo.getTexture().getWidth()/2,ofGetWindowHeight()/2 - toastLogo.getTexture().getHeight()/2);
//
//        ofPushStyle();
//        ofSetColor(0,0,0);
//        pointTracer.draw(logoPoints.getPoints());
//        ofPopStyle();
//    }

    flowDisplay.draw();

    beatDisplacementMap.draw(audioPlayer.getPointValues());
    basicBeat.draw();
    
    if(screengrabBool){
        takeScreenshot();
    }

    if(showGuiBool) {
        ofPushStyle();
        ofEnableBlendMode(OF_BLENDMODE_ALPHA);
        gui.draw();
        ofPopStyle();
        ofShowCursor();
    } else {
        ofHideCursor();
    }

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    switch(key) {
            case 'h':
                showGuiBool = !showGuiBool;
                break;
            case 'p':
                resetPositions();
                break;
            case 's':
                screengrabBool = true;
                break;
            case 'v':
                if(!videoRecordingBool)
                    recordVideo();
                break;
//            case 'f':
//                ofToggleFullscreen();
//                break;
            case ' ':
                audioPlayer.playPauseHandler();
                break;
    }
}

void ofApp::takeScreenshot() {
    screengrabImg.grabScreen(0,0, ofGetWidth(), ofGetHeight());
    screengrabImg.save("screengrabs/snapshot_"+ofGetTimestampString()+".png");
    screengrabBool = false;
}

void ofApp::recordVideo() {
    showGuiStore = showGuiBool;
    showGuiBool = false;
    videoRecordingBool = true;
    
    int sampleRate = 16000;
    int channels = 2;
    
    string fileName = "screenrecord_"+ofGetTimestampString();
    string fileExt = ".mov";
    
    vidRecorder.setVideoCodec("mpeg4");
    vidRecorder.setVideoBitrate("800k");
    vidRecorder.setAudioCodec("mp3");
    vidRecorder.setAudioBitrate("192k");
    
    ofAddListener(vidRecorder.outputFileCompleteEvent, this, &ofApp::recordingComplete);
    
    
//    soundStream.printDeviceList();
//    soundStream.setOu
//    soundStream.setDeviceID(1);
//    soundStream.setInput(this);
    soundStream.setup(this, 2, 0, sampleRate, 256, 4);
    
    vidRecorder.setup(fileName+fileExt, 640, 480, 30, sampleRate, channels);
    vidRecorder.start();
    
    _timer = new ofxSimpleTimer();
    
    _timer->setTime(15000,1);
    _timer->setName("recordingTimer");
    _timer->start();
    
    ofAddListener(ofxSimpleTimer::TIMER_COMPLETE, this, &ofApp::stopRecording);
    
}

//--------------------------------------------------------------
void ofApp::audioIn(float *input, int bufferSize, int nChannels){
    if(videoRecordingBool)
        vidRecorder.addAudioSamples(input, bufferSize, nChannels);
}

void ofApp::stopRecording(string & name) {
    _timer->pause();
    _timer->reset();
    vidRecorder.close();
    videoRecordingBool = false;
    showGuiBool = showGuiStore;
}

void ofApp::recordingComplete(ofxVideoRecorderOutputFileCompleteEventArgs& args){
    cout << "The recoded video file is now complete." << endl;
}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
//    pointTracer.pointClicked(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    // need to update the sizing of the fbos and such
//    beatDisplacementMap.updateSize(w,h);
//    
//    int preX = centerX;
//    int preY = centerY;
//    
//    int midX = int(w*0.5);
//    int midY = int(h*0.5);
//    
//    centerX.set("Center X",preX,-midX,midX);
//    centerY.set("Center Y",preY,-midY,midY);
//    
//    updateCenters();
}

void ofApp::updateCenters() {
    int pointX = rint(ofGetWindowWidth()/2 + centerX);
    int pointY = rint(ofGetWindowHeight()/2 + centerY);
    logoPoints.setCenter(pointX, pointY);
    beatDisplacementMap.setCenter(pointX, pointY);
//    pointTracer.setCenter(pointX, pointY);
    basicBeat.setCenter(pointX, pointY);
}

void ofApp::resetPositions() {
    centerX = 0;
    centerY = 0;
    updateCenters();
}


//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::exit(){
    ofRemoveListener(vidRecorder.outputFileCompleteEvent, this, &ofApp::recordingComplete);
    vidRecorder.close();
	
	basicBeat.cleanup();
}
