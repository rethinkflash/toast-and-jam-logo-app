//
//  logoPoints.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-10.
//
//

#ifndef logoPoints_h
#define logoPoints_h

#include "ofMain.h"

class LogoPoints {
protected:
    void addPoint(int x, int y);
    void allocateRows(int len);
    void startNewRow();
    void endNewRow();
    int currentRowPos;
    vector<ofVec2f>* currentRow;
    
public:
    void setup();
    void loadPoints();
    int getPointCount();
    int getRowCount();
    void setCenter(int x, int y);
    vector < vector<ofVec2f> > getPoints();
    
    ofVec2f screenCenter;
    vector< vector <ofVec2f> > vertPoints;
};

#endif /* logoPoints_h */
