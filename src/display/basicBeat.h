//
//  basicBeat.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-11.
//
//

#ifndef basicBeat_h
#define basicBeat_h

#include "ofMain.h"
#include "ofxGui.h"
#include "audioPlayer.h"
#include "ofxBox2d.h"

class BasicBeat {
protected:
    void addPoint(int x, int y);

public:
    void setup(vector < vector<ofVec2f> > rows, ofVec2f centerOffset);
	void createBlob(float posX, float posY);
    void initParams();
	void update(vector < vector<ofVec2f> > rows, ofPixels* pixels, ofVec2f centerOffset);
    void draw();
    void setScale(float newScale);
    void setCenter(int x, int y);
	void cleanup();
	
	int rowSize;
	float sizeValue;
	int pixelIndex;
	
	int row_i;
	int point_j;

    ofTexture getTexture();

    ofVec2f screenCenter;
    vector<ofVec2f>* vertPoints;
	ofColor dotColor;
	
	int blobUpdate_i;
	int blobDraw_i;
	vector<shared_ptr<ofxBox2dCircle>> blobAnchorVector;
	vector<shared_ptr<ofxBox2dCircle>> blobEdgeVector;
	vector<shared_ptr<ofxBox2dJoint>> blobJointVector;
	b2Filter CollisionFilter;
	ofxBox2d box2d;
	
    float scale;
    float posX;
    float posY;
	float radius;

    float bumpDistanceMultiplierX;
    float bumpDistanceMultiplierY;

    ofFbo dotFbo;
    ofFbo fullFbo;

    ofVec2f* thisPoint;
    
    ofParameterGroup parameters;
    ofParameter<bool> displayToggle;
    
    ofParameter<int> audioPointOffset;
    
    
    ofParameterGroup colorParameters;
    ofParameter<ofColor> pointMinColor;
    ofParameter<ofColor> pointMaxColor;
    
    
    ofParameterGroup positionParameters;
    ofParameter<float> bumpDistanceMultiplierXSlider;
    void setBumpDistanceMultiplierXField(float& _value) {
        bumpDistanceMultiplierX = _value;
    }
    ofParameter<float> bumpDistanceMultiplierYSlider;
    void setBumpDistanceMultiplierYField(float& _value) {
        bumpDistanceMultiplierY = _value;
    }
    ofParameter<float> pointMinSize;
    ofParameter<float> pointMaxSize;
    ofParameter<float> displayScale;
    ofParameter<int> dotResolution;
    void setDotResolutionField(int& _value) {
        ofSetCircleResolution(_value);
    }
    
private:
    void allocateFbos(int width, int height);
};


#endif /* basicBeat_h */
