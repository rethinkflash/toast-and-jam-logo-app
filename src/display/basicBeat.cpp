//
//  basicBeat.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-11.
//
//

#include "basicBeat.h"

void BasicBeat::setup(vector < vector<ofVec2f> > rows, ofVec2f centerOffset) {

    screenCenter.set(0,0);
    scale = 1.0;
	
	initParams();
    
    allocateFbos(ofGetWindowWidth(),ofGetWindowHeight());
	
	box2d.init();
	box2d.setGravity(0, -30);
	box2d.setFPS(30.0);
//	box2d.getWorld()->DestroyBody()
	CollisionFilter.groupIndex = -1;
	
	int midW = ofGetWindowWidth()*0.5;
	int midH = ofGetWindowHeight()*0.5;
	
	rowSize = rows.size();
	
	for(row_i = 0; row_i < rowSize; row_i++) {
		vertPoints = &rows[row_i];
		int pointSize = vertPoints->size();
		for(int point_j = 0; point_j < pointSize; point_j++) {
			
			
			thisPoint = &vertPoints->at(point_j);
			
			posX = (displayScale
				* thisPoint->x)
				+ midW;
			
			posY = (displayScale
				* thisPoint->y)
				+ midH;
			
			createBlob(posX, posY);
			
//			ofDrawCircle(posX, posY, ofMap(sizeValue,0,1.0,pointMinSize,pointMaxSize));
		}
	}
	
	
	
}

void BasicBeat::createBlob(float posX, float posY) {
	shared_ptr<ofxBox2dCircle> tempBlobAnchor = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
	tempBlobAnchor.get()->setup(box2d.getWorld(), posX, posY, 5);
	tempBlobAnchor.get()->setFilterData(CollisionFilter);
	blobAnchorVector.push_back(tempBlobAnchor);
	
	int res = 6;
	float radius = 30.0;
	float popX = 0.;
	float popY = 0.0;
	float angleDist = (float)res/360;
	
	for(int i = 0; i < res; i++){
		shared_ptr<ofxBox2dCircle> tempBlobEdge = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
		shared_ptr<ofxBox2dJoint> tempAnchorJoint = shared_ptr<ofxBox2dJoint>(new ofxBox2dJoint);
		popX = (radius * cos(angleDist*i));
		popY = (radius * sin(angleDist*i));
		
		
		tempBlobEdge.get()->setup(box2d.getWorld(), popX, popY, 3);
		tempBlobEdge.get()->setFilterData(CollisionFilter);
		tempBlobEdge.get()->setPhysics(0.5,0.5,0.5);
		blobEdgeVector.push_back(tempBlobEdge);
		
		tempAnchorJoint.get()->setup(box2d.getWorld(), tempBlobAnchor.get()->body, tempBlobEdge.get()->body);
		tempAnchorJoint.get()->setLength(5);
		tempAnchorJoint.get()->setDamping(0.5);
		blobJointVector.push_back(tempAnchorJoint);
	}
}

void BasicBeat::initParams() {
    parameters.setName("Basic Beat");
    parameters.add(displayToggle.set("Display",true));
    
    colorParameters.setName("Colors");
    colorParameters.add(pointMinColor.set("Min Point Color",ofColor(255,255,255,255)));
    colorParameters.add(pointMaxColor.set("Max Point Color",ofColor(255,255,255,255)));
    
    
    positionParameters.setName("Scaling");
    positionParameters.add(bumpDistanceMultiplierXSlider.set("Bump Dist X",30.0,0.0,300));
    bumpDistanceMultiplierXSlider.addListener(this, &BasicBeat::setBumpDistanceMultiplierXField);
    positionParameters.add(bumpDistanceMultiplierYSlider.set("Bump Dist Y",30.0,0.0,300));
    bumpDistanceMultiplierYSlider.addListener(this, &BasicBeat::setBumpDistanceMultiplierYField);
    positionParameters.add(pointMinSize.set("Dot Min Size", 1.0, 0.5, 10.0));
    positionParameters.add(pointMaxSize.set("Dot Max Size", 10.0, 1.0, 30.0));
    positionParameters.add(displayScale.set("Display Scale", 1.0, 0.5, 5.0));
    positionParameters.add(dotResolution.set("Dot Resolution", 8, 3, 20));
    dotResolution.addListener(this, &BasicBeat::setDotResolutionField);
    ofSetCircleResolution(dotResolution);
                              
    parameters.add(colorParameters);
    parameters.add(positionParameters);
    parameters.add(audioPointOffset.set("Spectrum Offset",2,0,100));
    
}

void BasicBeat::setScale(float newScale) {
    scale = newScale;
}

void BasicBeat::update(vector < vector<ofVec2f> > rows, ofPixels* pixels, ofVec2f centerOffset) {

	sizeValue = 0.0;
	rowSize = rows.size();
	pixelIndex = 0;
	blobUpdate_i = 0;
	
	for(row_i = 0; row_i < rowSize; row_i++) {
		vertPoints = &rows[row_i];
		int pointSize = vertPoints->size();
		for(int point_j = 0; point_j < pointSize; point_j++) {
			
			
			thisPoint = &vertPoints->at(point_j);
			
			sizeValue = (float)ofMap(pixels->getColor((displayScale * thisPoint->x) + screenCenter.x, (displayScale * thisPoint->y) + screenCenter.y).r,0,255,0,1.0);
			
			posX = displayScale
			* (ofMap(sizeValue, 0, 100, thisPoint->x,
					 (bumpDistanceMultiplierX)
					 * (thisPoint->x - centerOffset.x) ))
			+ screenCenter.x;
			
			posY = displayScale
			* (ofMap(sizeValue, 0, 100, thisPoint->y,
					 (bumpDistanceMultiplierY)
					 * (thisPoint->y - centerOffset.y) ))
			+ screenCenter.y;
			
			dotColor.set(
					   (int)ofMap(sizeValue, 0.0, 1.0, pointMinColor->r,pointMaxColor->r),
					   (int)ofMap(sizeValue, 0.0, 1.0, pointMinColor->g,pointMaxColor->g),
					   (int)ofMap(sizeValue, 0.0, 1.0, pointMinColor->b,pointMaxColor->b),
					   (int)ofMap(sizeValue, 0.0, 1.0, pointMinColor->a,pointMaxColor->a)
					   );
			
			
			blobAnchorVector.at(blobUpdate_i).get()->setPosition(posX, posY);
				//		softDots.at(softDot_i)->draw(false, true);
			
			blobUpdate_i++;

//			softDots.at(softDot_i)->update(&dotColor, sizeValue, posX, posY);
//			softDot_i++;
//			ofDrawCircle(posX, posY, ofMap(sizeValue,0,1.0,pointMinSize,pointMaxSize));
		}
	}
	
	box2d.update();
	
}


void BasicBeat::draw() {
	
	ofPushStyle();
	ofEnableBlendMode(OF_BLENDMODE_DISABLED);
	
	if(!dotFbo.isAllocated())
		return;
	
	dotFbo.begin();
	dotFbo.setAnchorPoint(0, 0);
	ofClear(255,255,255,0);
	

	for(int i = 0; i < blobAnchorVector.size(); i++) {
		blobAnchorVector.at(i).get()->draw();
	}
	
	for(int i = 0; i < blobEdgeVector.size(); i++) {
		blobEdgeVector.at(i).get()->draw();
	}
	
	for(int i = 0; i < blobJointVector.size(); i++) {
		blobJointVector.at(i).get()->draw();
	}
	
	dotFbo.end();
	ofPopStyle();
	
	ofPushStyle();
	
	if(!fullFbo.isAllocated())
		return;
	
	fullFbo.begin();
	
	ofPushStyle();
	ofSetColor(0, 0,0,20);
	ofDrawRectangle(0.0, 0.0, ofGetWindowWidth(), ofGetWindowHeight());
	ofPopStyle();
	
	dotFbo.draw(0,0);
	fullFbo.end();
	ofPopStyle();
//  fullFbo.draw(0,0);
	
	if(displayToggle) {
		dotFbo.draw(0, 0);
	}
}

ofTexture BasicBeat::getTexture() {
    return fullFbo.getTexture();
}

void BasicBeat::setCenter(int x, int y) {
    screenCenter.set(x,y);
}

void BasicBeat::allocateFbos(int width, int height) {
    dotFbo.allocate(width,height,GL_RGBA, 4);
    dotFbo.begin();
    ofClear(255,255,255,0);
    dotFbo.end();
	
    fullFbo.allocate(width,height,GL_RGB);
    fullFbo.begin();
    ofClear(255,255,255,0);
    fullFbo.end();
}

void BasicBeat::cleanup() {
	
}
