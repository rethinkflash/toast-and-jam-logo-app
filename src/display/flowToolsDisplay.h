//
//  flowToolsDisplay.h
//  toast-and-jam
//
//  Created by Ken Malley on 2016-09-09.
//
//

#ifndef flowToolsDisplay_h
#define flowToolsDisplay_h

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxFlowTools.h"

using namespace flowTools;

enum drawModeEnum{
    DRAW_FLUID_DENSITY = 0,
    DRAW_PARTICLES,
    DRAW_FLUID_VELOCITY,
    DRAW_FLUID_TEMPERATURE,
    DRAW_OPTICAL_FLOW
};

class FlowToolsDisplay {
	
public:
    void setup();
    void initParams();
    void update(ofTexture textureSrc);
    void draw();

    // FlowTools
    int flowWidth;
    int flowHeight;
    int drawWidth;
    int drawHeight;
    
    ofParameterGroup parameters;

    // used to calculate simulations, not display
    ftOpticalFlow		opticalFlow;
    ftVelocityMask		velocityMask;
    ftFluidSimulation	fluidSimulation;
    ftParticleFlow		particleFlow;

    // Visualisations
    // ofParameterGroup	visualizeParameters;
    ftDisplayScalar		displayScalar;
    ftVelocityField		velocityField;
     ftTemperatureField	temperatureField;
    // ftPressureField		pressureField;
    // ftVTField			velocityTemperatureField;
    
    ofParameter<bool>	showScalar;
    ofParameter<bool>	showField;
    ofParameter<float>	displayScalarScale;
    
    void setDisplayScalarScale(float& _value) { displayScalar.setScale(_value);}

    // GUI
    ofParameter<ofColor> backgroundColor;
    ofParameter<int> drawMode;
    ofParameter<bool> addObstacle;
    void drawModeSetName(int& _value);
    ofParameter<string> drawName;

    // Draw methods
    void drawFluidDensity() { drawFluidDensity(0, 0, ofGetWindowWidth(), ofGetWindowHeight()); }
    void drawFluidDensity(int _x, int _y, int _width, int _height);
    
    void drawParticles() { drawParticles(0, 0, ofGetWindowWidth(), ofGetWindowHeight()); }
    void drawParticles(int _x, int _y, int _width, int _height);

    void drawFluidVelocity() { drawFluidVelocity(0, 0, ofGetWindowWidth(), ofGetWindowHeight()); }
    void drawFluidVelocity(int _x, int _y, int _width, int _height);

    void drawFluidTemperature()	{ drawFluidTemperature(0, 0, ofGetWindowWidth(), ofGetWindowHeight()); }
    void drawFluidTemperature(int _x, int _y, int _width, int _height);

    void drawOpticalFlow() { drawOpticalFlow(0, 0, ofGetWindowWidth(), ofGetWindowHeight()); }
    void drawOpticalFlow(int _x, int _y, int _width, int _height);

private:
    void allocateVisuals(int width, int height);
};

#endif /* flowToolsDisplay_h */
