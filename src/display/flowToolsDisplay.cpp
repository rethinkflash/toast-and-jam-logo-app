//
//  flowToolsDisplay.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-09-09.
//
//

#include "flowToolsDisplay.h"

void FlowToolsDisplay::setup() {

    allocateVisuals(ofGetWindowWidth(), ofGetWindowHeight());
    
    initParams();
}

void FlowToolsDisplay::allocateVisuals(int width, int height) {
    drawWidth = (int)width/2;
    drawHeight = (int)height/2;
    
    int densityDivider = 2;
    
    //process all but the density on 16th resolution
    flowWidth = drawWidth/densityDivider;
    flowHeight = drawHeight/densityDivider;
    
    // Flow & Mask
    opticalFlow.setup(flowWidth, flowHeight);
    velocityMask.setup(drawWidth, drawHeight);
    
    // Fluid & Particles
    fluidSimulation.setup(flowWidth, flowHeight, drawWidth, drawHeight);
    particleFlow.setup(flowWidth, flowHeight, drawWidth, drawHeight);
    
    // VISUALIZATION
    displayScalar.setup(flowWidth, flowHeight);
    velocityField.setup(flowWidth / 2, flowHeight / 2);
    temperatureField.setup(flowWidth / 2, flowHeight / 2);
	
    // pressureField.setup(flowWidth / 4, flowHeight / 4);
    // velocityTemperatureField.setup(flowWidth / 4, flowHeight / 4);
}

void FlowToolsDisplay::initParams() {
    
    parameters.setName("ofxFlowTools");
    
    parameters.add(addObstacle.set("add obstacle",false));
    parameters.add(showScalar.set("show scalar", true));
    parameters.add(showField.set("show field", true));
    parameters.add(drawMode.set("draw mode", DRAW_FLUID_DENSITY, DRAW_FLUID_DENSITY, DRAW_OPTICAL_FLOW));
    parameters.add(drawName.set("MODE", "Fluid Density  (1)"));
    
    parameters.add(opticalFlow.parameters);
    parameters.add(fluidSimulation.parameters);
    parameters.add(particleFlow.parameters);
    parameters.add(velocityMask.parameters);

    drawMode.addListener(this, &FlowToolsDisplay::drawModeSetName);

//    mainPanel->add(&flowToolsFolder);
}

void FlowToolsDisplay::drawModeSetName(int &_value) {
    switch(_value) {
        case DRAW_FLUID_DENSITY:	drawName.set("Fluid Density  (1)"); break;
        case DRAW_PARTICLES:		drawName.set("Particles      (2)"); break;
        case DRAW_FLUID_VELOCITY:	drawName.set("Fluid Velocity (3)"); break;
        case DRAW_FLUID_TEMPERATURE:drawName.set("Fld Temperature (4)"); break;
        case DRAW_OPTICAL_FLOW:		drawName.set("Optical Flow   (5)"); break;
    }
}

void FlowToolsDisplay::update(ofTexture textureSrc) {

    opticalFlow.setSource(textureSrc);
    opticalFlow.update();

    velocityMask.setDensity(textureSrc);
    velocityMask.setVelocity(opticalFlow.getOpticalFlow());
    velocityMask.update();
    
    if(addObstacle) {
        fluidSimulation.addTempObstacle(textureSrc);
    }
    
    fluidSimulation.addVelocity(opticalFlow.getOpticalFlowDecay());
    fluidSimulation.addDensity(velocityMask.getColorMask());
    fluidSimulation.addTemperature(velocityMask.getLuminanceMask());

    fluidSimulation.update();
    
    if(particleFlow.isActive()) {
        particleFlow.setSpeed(fluidSimulation.getSpeed());
        particleFlow.setCellSize(fluidSimulation.getCellSize());
        particleFlow.addFlowVelocity(opticalFlow.getOpticalFlow());
        particleFlow.addFluidVelocity(fluidSimulation.getVelocity());
//		particleFlow.addDensity(fluidSimulation.getDensity());
        particleFlow.setObstacle(fluidSimulation.getObstacle());
    }
    
    particleFlow.update();
}

void FlowToolsDisplay::draw() {
    switch(drawMode.get()) {
		case DRAW_FLUID_DENSITY: drawFluidDensity(); break;
        case DRAW_PARTICLES: drawParticles(); break;
        case DRAW_FLUID_VELOCITY: drawFluidVelocity(); break;
        case DRAW_FLUID_TEMPERATURE: drawFluidTemperature(); break;
        case DRAW_OPTICAL_FLOW: drawOpticalFlow(); break;
    }
}

void FlowToolsDisplay::drawFluidDensity(int _x, int _y, int _width, int _height) {
//    ofPushStyle();
//    ofSetColor(backgroundColor);
//    ofDrawRectangle(0.0, 0.0, ofGetWindowWidth(), ofGetWindowHeight());
//    ofPopStyle();
    ofPushStyle();
    // it is running here
    fluidSimulation.draw(_x, _y, _width, _height);
    ofPopStyle();
}

//--------------------------------------------------------------
void FlowToolsDisplay::drawParticles(int _x, int _y, int _width, int _height) {
//    ofPushStyle();
//    ofSetColor(backgroundColor);
//    ofDrawRectangle(0.0, 0.0, ofGetWindowWidth(), ofGetWindowHeight());
//    ofPopStyle();
    ofPushStyle();
    ofEnableBlendMode(OF_BLENDMODE_ALPHA);
    if (particleFlow.isActive())
        particleFlow.draw(_x, _y, _width, _height);
    ofPopStyle();
}

void FlowToolsDisplay::drawFluidVelocity(int _x, int _y, int _width, int _height) {
    ofPushStyle();
     if (showScalar.get()) {
        ofClear(0,0);
        ofEnableBlendMode(OF_BLENDMODE_ALPHA);
        //	ofEnableBlendMode(OF_BLENDMODE_DISABLED); // altenate mode
        displayScalar.setSource(fluidSimulation.getVelocity());
        displayScalar.draw(_x, _y, _width, _height);
     }
     if (showField.get()) {
         ofEnableBlendMode(OF_BLENDMODE_ADD);
        velocityField.setVelocity(fluidSimulation.getVelocity());
        velocityField.draw(_x, _y, _width, _height);
     }
    ofPopStyle();
}

void FlowToolsDisplay::drawFluidTemperature(int _x, int _y, int _width, int _height) {
    ofPushStyle();
     if (showScalar.get()) {
         ofEnableBlendMode(OF_BLENDMODE_DISABLED);
         displayScalar.setSource(fluidSimulation.getTemperature());
         displayScalar.draw(_x, _y, _width, _height);
     }
     if (showField.get()) {
         ofEnableBlendMode(OF_BLENDMODE_ADD);
         temperatureField.setTemperature(fluidSimulation.getTemperature());
         temperatureField.draw(_x, _y, _width, _height);
     }
    ofPopStyle();
}

void FlowToolsDisplay::drawOpticalFlow(int _x, int _y, int _width, int _height) {
    ofPushStyle();
     if (showScalar.get()) {
         ofEnableBlendMode(OF_BLENDMODE_ALPHA);
         displayScalar.setSource(opticalFlow.getOpticalFlowDecay());
         displayScalar.draw(0, 0, _width, _height);
     }
     if (showField.get()) {
         ofEnableBlendMode(OF_BLENDMODE_ADD);
         velocityField.setVelocity(opticalFlow.getOpticalFlowDecay());
         velocityField.draw(0, 0, _width, _height);
     }
    ofPopStyle();
}
