//
//  PointTracer.cpp
//  toast-and-jam
//
//  Created by Ken Malley on 2016-08-09.
//
//

#include "PointTracer.h"

void PointTracer::setup() {
    
}

void PointTracer::pointClicked(int x, int y) {
    //draws the clicked points
    
    ofVec2f point = ofVec2f( x - screenCenter.x, y - screenCenter.y );
    
    vertPoints.push_back(point);
}

void PointTracer::draw(vector < vector<ofVec2f> > vertPoints) {
    ofSetColor(46, 118, 154);
        for(auto &thisPoint : vertPoints) {
            ofDrawCircle(thisPoint.x + screenCenter.x, thisPoint.y +screenCenter.y , 3);
        }
    
    ofSetColor(255, 255, 255);
//    ofRectangle clickArea(0,0,ofGetWindowWidth(),ofGetWindowHeight());
    
}

void PointTracer::setCenter(int x, int y) {
    screenCenter.set(x,y);
}

void PointTracer::saveFile() {
    ofFile pointsFile(ofToDataPath("logopoints.xml"),ofFile::WriteOnly);
    pointsFile.create();
    pointsFile << "<points>\n";
    for(auto &thisPoint : vertPoints) {
        pointsFile << "<point x='" << thisPoint.x << "' y='" << thisPoint.y << "'/>\n";
    }
    pointsFile << "</points>\n";
    pointsFile.close();
    ofLog() << "logopoints.xml saved";
}